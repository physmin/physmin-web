import * as admin from 'firebase-admin'

export const firebaseConfig = {
    apiKey: "AIzaSyD591euTx_KHzty2GKcc8Z4j-Tq0N2jI2Q",
    authDomain: "physmin.firebaseapp.com",
    databaseURL: "https://physmin.firebaseio.com",
    projectId: "physmin",
    storageBucket: "physmin.appspot.com",
    messagingSenderId: "280499204418",
    appId: "1:280499204418:web:edcd00e28d353470fc98db",
    measurementId: "G-NNKTH8E859"
  };
  
  let app = admin.initializeApp()
  const db = app.firestore()

  export {app, db}