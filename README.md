# Service website for PhysMin app
![Screenshot of tool](res/screenshot.jpg)

This website used to managing tasks sequence generator configs of PhysMin app.

## Stack
- React
- Redux
- Typescript
- Material UI